NAME = libbase
SOURCES = file.cpp \
         chrono_utils.cpp \
         cmsg.cpp \
         logging.cpp \
         parsenetaddress.cpp \
         mapped_file.cpp \
         properties.cpp \
         quick_exit.cpp \
         stringprintf.cpp \
         strings.cpp \
         threads.cpp \
         test_utils.cpp
CXX := clang++
SOURCES := $(foreach source, $(SOURCES), base/$(source))
CXXFLAGS += -c -std=c++17
CPPFLAGS += -Iinclude -Ibase/include
LDFLAGS += -shared -Wl,-soname,$(NAME).so.0

build: $(COBJECTS) $(CXXOBJECTS)
	$(CXX) $^ -o $(NAME).so.0 $(LDFLAGS)
	ln -s $(NAME).so.0 $(NAME).so

clean:
	$(RM) $(CXXOBJECTS) $(COBJECTS) $(NAME).so*

$(COBJECTS): %.o: %.c
	$(CC) $< -o $@ $(CFLAGS) $(CPPFLAGS)

$(CXXOBJECTS): %.o: %.cpp
	$(CXX) $< -o $@  $(CXXFLAGS) $(CPPFLAGS)
