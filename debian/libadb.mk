NAME := libadb

LIBADB_SRC_FILES := \
    adb.cpp \
    adb_io.cpp \
    adb_listeners.cpp \
    adb_trace.cpp \
    adb_unique_fd.cpp \
    adb_utils.cpp \
    fdevent.cpp \
    services.cpp \
    sockets.cpp \
    socket_spec.cpp \
    sysdeps/errno.cpp \
    transport.cpp \
    transport_fd.cpp \
    transport_local.cpp \
    transport_usb.cpp \

LIBADB_linux_SRC_FILES := \
    client/usb_linux.cpp \

LIBADB_POSIX_SRC := \
	sysdeps_unix.cpp \
	sysdeps/posix/network.cpp\ 

SOURCES := \
    $(LIBADB_SRC_FILES) \
    $(LIBADB_linux_SRC_FILES) \

CC := clang
CXX := clang++
CSOURCES := $(foreach source, $(filter %.c, $(SOURCES)), adb/$(source))
CXXSOURCES := $(foreach source, $(filter %.cpp, $(SOURCES)), adb/$(source))
COBJECTS := $(CSOURCES:.c=.o)
CXXOBJECTS := $(CXXSOURCES:.cpp=.o)
CFLAGS += -c
CXXFLAGS += -c -std=c++17
CPPFLAGS += -I/usr/include/android -Iadb -Iinclude -Ibase/include \
	    -Ilibcrypto_utils/include -DADB_HOST=1 -DADB_VERSION='"$(DEB_VERSION)"'
LDFLAGS += -shared -Wl,-soname,$(NAME).so.0 \
           -Wl,-rpath=/usr/lib/$(DEB_HOST_MULTIARCH)/android \
           -L/usr/lib/$(DEB_HOST_MULTIARCH)/android -lcrypto \
           -lpthread -latomic -L. -lbase -lcutils -lcrypto_utils -lusb-1.0

build: $(COBJECTS) $(CXXOBJECTS)
	$(CXX) $^ -o $(NAME).so.0 $(LDFLAGS)
	ln -s $(NAME).so.0 $(NAME).so

clean:
	$(RM) $(CXXOBJECTS) $(COBJECTS) $(NAME).so*

$(COBJECTS): %.o: %.c
	$(CC) $< -o $@ $(CFLAGS) $(CPPFLAGS)

$(CXXOBJECTS): %.o: %.cpp
	$(CXX) $< -o $@  $(CXXFLAGS) $(CPPFLAGS)