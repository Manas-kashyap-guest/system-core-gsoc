# copied from libcrypto_utils/Android.bp
NAME:= libcrypto_utils
SOURCES := android_pubkey.c
CSOURCES := $(foreach source, $(filter %.c, $(SOURCES)), libcrypto_utils/$(source))
CXXSOURCES := $(foreach source, $(filter %.cpp, $(SOURCES)), libcrypto_utils/$(source))
CPPFLAGS += -Ilibcrypto_utils/include -Iinclude -I/usr/include/android
LDFLAGS += -shared -Wl,-soname,$(NAME).so.0 \
	-Wl,-rpath=/usr/lib/$(DEB_HOST_MULTIARCH)/android \
	-L/usr/lib/$(DEB_HOST_MULTIARCH)/android \
	-lcrypto -Wl,-z,defs
COBJECTS := $(CSOURCES:.c=.o)
CXXOBJECTS := $(CXXSOURCES:.cpp=.o)
CFLAGS += -c
CXXFLAGS += -c -std=c++17
CC := clang
CXX := clang++
build: $(COBJECTS) $(CXXOBJECTS)
	$(CXX) $^ -o $(NAME).so.0 $(LDFLAGS)
	ln -s $(NAME).so.0 $(NAME).so

clean:
	$(RM) $(CXXOBJECTS) $(COBJECTS) $(NAME).so*

$(COBJECTS): %.o: %.c
	$(CC) $< -o $@ $(CFLAGS) $(CPPFLAGS)

$(CXXOBJECTS): %.o: %.cpp
	$(CXX) $< -o $@  $(CXXFLAGS) $(CPPFLAGS)
